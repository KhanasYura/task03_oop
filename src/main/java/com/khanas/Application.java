package com.khanas;

import com.khanas.view.MyView;

/**
 * Task.
 * Create a housing hierarchy, such as a mansion, a flat, a penthouse, etc.
 * To make offers of available housing in Lviv for the buyer.
 * Consider the location of housing in social infrastructure (kindergartens,
 * schools, playgrounds) Search housing by certain parameters
 */
public final class Application {
    /**
     * Application - private constructor.It isn`t needed here.
     */
    private Application() {
        throw new AssertionError("Instantiating utility class.");
    }

    /**
     * Start of the application.
     * @param args default parameters
     */
    public static void main(final String[] args) {
        new MyView();
    }
}
