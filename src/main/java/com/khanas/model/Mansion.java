package com.khanas.model;
/**
 * Mansion.
 * Class Mansion extends House
 * Info about Mansions
 */
public class Mansion extends House {
    /**
     * numberOfFloors - info about the number.
     * of floors mansion has
     */
    private int numbersOfFloors;
    /**
     * garageNumber - info about the number of.
     * parking spaces
     */
    private int garageNumber;
    /**
     * Mansion - constructor.
     * @param squareMeters - info about how many
     *                    square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param numbersOfFloors1 - info about the number;
     *                           of floors mansion has
     * @param garageNumber1 - info about the number of
     *                        parking spaces
     */
    public Mansion(final double  squareMeters,
                   final double distanceToKindergarten,
                   final double distanceToSchool,
                   final double distanceToCenter,
                   final double distanceToMall,
                   final double price, final double room,
                   final int numbersOfFloors1,
                   final int garageNumber1) {
        super(squareMeters, distanceToKindergarten,
                distanceToSchool, distanceToCenter,
                distanceToMall, price, room);
        this.numbersOfFloors = numbersOfFloors1;
        this.garageNumber = garageNumber1;
    }

    /**
     * toString().
     * @return info about a mansion
     */
    @Override
    public final String toString() {
        return "Mansion " + super.toString()
                + ", numbersOfFloors=" + numbersOfFloors
                + ", garageNumber=" + garageNumber;
    }

    /**
     * getNumbersOfFloors.
     * @return info about the number of floors mansion has
     */
    public final int getNumbersOfFloors() {
        return numbersOfFloors;
    }

    /**
     * setNumbersOfFloors.
     * @param numbersOfFloors1 - info about the number of floors
     */
    public final void setNumbersOfFloors(final int numbersOfFloors1) {
        this.numbersOfFloors = numbersOfFloors1;
    }

    /**
     * getGarageNumber.
     * @return info about number of garage spaces
     */
    public final int getGarageNumber() {
        return garageNumber;
    }

    /**
     * srtGarageNumber.
     * @param garageNumber1 - info about number garage spaces
     */
    public final void setGarageNumber(final int garageNumber1) {
        this.garageNumber = garageNumber1;
    }
}
