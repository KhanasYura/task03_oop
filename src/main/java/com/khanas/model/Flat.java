package com.khanas.model;

/**
 * Flat.
 * Class Flat extends House
 * Info about flats
 */
public class Flat extends House {
    /**
     * floor - variable contains info;
     * on which floor flat is located.
     */
    private int floor;
    /**
     * Flat - constructor.
     * @param squareMeters - info about how many
     *                    square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param floor1 -info;
     *               on which floor flat is located.
     */
    public Flat(final double squareMeters,
                final double distanceToKindergarten,
                final double distanceToSchool,
                final double distanceToCenter,
                final double distanceToMall,
                final double price,
                final double room, final int floor1) {
        super(squareMeters, distanceToKindergarten,
                distanceToSchool, distanceToCenter,
                distanceToMall, price, room);
        this.floor = floor1;
    }

    /**
     * toString().
     * @return info about a flat;
     */
    @Override
    public final String toString() {
        return "Flat " + super.toString()
                + ", floor=" + floor;
    }
    /**
     * getFloor().
     * @return info about floor
     */
    public final  int getFloor() {
        return floor;
    }
    /**
     * setFloor().
     * Add info about floor
     * @param floor1 info about the floor
     */
    public final void setFloor(final int floor1) {
        this.floor = floor1;
    }
}
