package com.khanas.model;

/**
 * House.
 * Class House - parent class;
 * Info about houses;
 */
public class House {
    /**
     * squareMeters - info about how many.
     * square meters have the penthouse;
     */
    private double squareMeters;
    /**
     * distanceToKindergarten - info about distance.
     * to kindergarten;
     */
    private double distanceToKindergarten;
    /**
     * distanceToSchool - info about distance.
     * to school;
     */
    private double distanceToSchool;
    /**
     * distanceToCenter - info about distance.
     * to center;
     */
    private double distanceToCenter;
    /**
     * distanceToMall - info about distance.
     * to mall;
     */
    private double distanceToMall;
    /**
     * price - info about the price of penthouse.
     */
    private double price;
    /**
     * room - info about number of rooms.
     */
    private double room;

    /**
     * House - constructor.
     * @param squareMeters1 - info about how many;
     *                    square meters have the penthouse;
     * @param distanceToKindergarten1 - info about distance;
     *                                 to kindergarten;
     * @param distanceToSchool1 - info about distance;
     *                           to school;
     * @param distanceToCenter1 - info about distance;
     *                           to center;
     * @param distanceToMall1 - info about distance;
     *                         to mall;
     * @param price1 - info about the price of penthouse;
     * @param room1 - info about number of rooms;
     */
    public House(final double squareMeters1,
                 final double distanceToKindergarten1,
                 final double distanceToSchool1,
                 final double distanceToCenter1,
                 final double distanceToMall1,
                 final double price1, final double room1) {
        this.squareMeters = squareMeters1;
        this.distanceToKindergarten = distanceToKindergarten1;
        this.distanceToSchool = distanceToSchool1;
        this.distanceToCenter = distanceToCenter1;
        this.distanceToMall = distanceToMall1;
        this.price = price1;
        this.room = room1;
    }

    /**
     * toString().
     * @return info about a house
     */
    public String toString() {
        return  "squareMeters=" + squareMeters
                + ", distanceToKindergarten=" + distanceToKindergarten
                + ", distanceToSchool=" + distanceToSchool
                + ", distanceToCenter=" + distanceToCenter
                + ", distanceToMall=" + distanceToMall
                + ", price=" + price
                + ", room=" + room;
    }

    /**
     * getSquareMeters.
     * @return info about square meters;
     */
    public final double getSquareMeters() {
        return squareMeters;
    }

    /**
     * setSquareMeters.
     * @param squareMeters1 - info about the square meters
     */
    public final void setSquareMeters(final double squareMeters1) {
        this.squareMeters = squareMeters1;
    }

    /**
     * getDistanceToKindergarten.
     * @return info about distance to kindergarten;
     */
    public final double getDistanceToKindergarten() {
        return distanceToKindergarten;
    }

    /**
     * setDistanceToKindergarten.
     * @param distanceToKindergarten1 -  info about distance to kindergarten;
     */
    public final void setDistanceToKindergarten(
            final double distanceToKindergarten1) {
        this.distanceToKindergarten = distanceToKindergarten1;
    }

    /**
     * getDistanceToSchool.
     * @return info about distance to school;
     */
    public final double getDistanceToSchool() {
        return distanceToSchool;
    }

    /**
     * setDistanceToSchool.
     * @param distanceToSchool1 - info about distance to school;
     */
    public final void setDistanceToSchool(final double distanceToSchool1) {
        this.distanceToSchool = distanceToSchool1;
    }

    /**
     * getDistanceToCenter.
     * @return info about distance to center;
     */
    public final double getDistanceToCenter() {
        return distanceToCenter;
    }

    /**
     * setDistanceToSchool.
     * @param distanceToCenter1 - info about distance to center;
     */
    public final void setDistanceToCenter(final double distanceToCenter1) {
        this.distanceToCenter = distanceToCenter1;
    }

    /**
     * getDistanceToMall.
     * @return info about distance to mall;
     */
    public final double getDistanceToMall() {
        return distanceToMall;
    }

    /**
     * setDistanceToMall.
     * @param distanceToMall1  info about distance to mall;
     */
    public final void setDistanceToMall(final double distanceToMall1) {
        this.distanceToMall = distanceToMall1;
    }

    /**
     * getPrice.
     * @return info about the price of the house;
     */
    public final double getPrice() {
        return price;
    }

    /**
     * setPrice.
     * @param price1 - info about the price of the house;
     */
    public final void setPrice(final double price1) {
        this.price = price1;
    }

    /**
     * getRoom.
     * @return info about the number of rooms;
     */
    public final double getRoom() {
        return room;
    }

    /**
     * setRoom.
     * @param room1 - info about the number of rooms;
     */
    public final void setRoom(final double room1) {
        this.room = room1;
    }
}

