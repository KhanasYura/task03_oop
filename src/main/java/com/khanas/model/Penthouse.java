package com.khanas.model;

/**
 * Penthouse.
 * Class Penthouse extends House
 * Info about penthouses
 */
public class Penthouse extends House {
    /**
     * numberOfFloors - variable contains info;
     * on which floor penthouse is located.
     */
    private int numberOfFloors;

    /**
     * Penthouse - constructor.
     * @param squareMeters - info about how many
     *                    square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param floor1 -info;
     *               on which floor penthouse is located.
     */
    public Penthouse(final double squareMeters,
                     final double distanceToKindergarten,
                     final double distanceToSchool,
                     final double distanceToCenter,
                     final double distanceToMall,
                     final double price,
                     final double room,
                     final int floor1) {
        super(squareMeters, distanceToKindergarten, distanceToSchool,
                distanceToCenter, distanceToMall, price, room);
        this.numberOfFloors = floor1;
    }

    /**
     * toString().
     * @return info about a penthouse
     */
    @Override
    public final String toString() {
        return "Penthouse " + super.toString()
                + ", floor=" + numberOfFloors;
    }
    /**
     * getFloor().
     * @return info about the floor
     */
    public final int getFloor() {
        return numberOfFloors;
    }
    /**
     * setFloor().
     * Add info about the floor
     * @param floor1 info about the floor
     */
    public final void setFloor(final int floor1) {
        this.numberOfFloors = floor1;
    }
}
