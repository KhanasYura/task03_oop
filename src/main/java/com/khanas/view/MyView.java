package com.khanas.view;

import com.khanas.controller.Controller;
import com.khanas.controller.ControllerInter;
import com.khanas.model.House;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * MyView class which is part of MVC pattern.
 * Here is a menu of application.
 */
public class MyView {
    /**
     * controller - object.
     */
    private final ControllerInter controller;
    /**
     * sc - Scanner.
     */
    private Scanner sc = new Scanner(System.in);
    /**
     * menuindex0 - value menu index.
     */
    private static final int MENUINDEX0 = 0;
    /**
     * menuindex1 - value menu index.
     */
    private static final int MENUINDEX1 = 1;
    /**
     * menuindex2 - value menu index.
     */
    private static final int MENUINDEX2 = 2;
    /**
     * menuindex3 - value menu index.
     */
    private static final int MENUINDEX3 = 3;
    /**
     * menuindex4 - value menu index.
     */
    private static final int MENUINDEX4 = 4;
    /**
     * menuindex5 - value menu index.
     */
    private static final int MENUINDEX5 = 5;
    /**
     * menuindex6 - value menu index.
     */
    private static final int MENUINDEX6 = 6;
    /**
     * menuindex7 - value menu index.
     */
    private static final int MENUINDEX7 = 7;
    /**
     * menuindex8 - value menu index.
     */
    private static final int MENUINDEX8 = 8;
    /**
     * menuindex9 - value menu index.
     */
    private static final int MENUINDEX9 = 9;
    /**
     * menuindex10 - value menu index.
     */
    private static final int MENUINDEX10 = 10;
    /**
     * list - list of houses.
     */
    private ArrayList<House> list = new ArrayList<House>();
    /**
     * autoMode - autocomplete trigger variable.
     */
    private boolean autoMode = true;

    /**
     * MyView is a constructor.
     * Here user can interact with a program
     */
    public MyView() {
        controller = new Controller();
        boolean firstmenu = true;
        if (autoMode) {
            try {
                sc = new Scanner(new BufferedInputStream(
                        new FileInputStream("automode.txt")));
            } catch (FileNotFoundException ex) {
                System.err.println("Автоматичний режим не запущено!");
                sc = new Scanner(System.in);
            }
        }
        while (firstmenu) {
            System.out.println("1.Add Flat");
            System.out.println("2.Add Penthouse");
            System.out.println("3.Add Mansion");
            System.out.println("4.Features");
            System.out.println("5.Exit");
            int menu = sc.nextInt();
            if (menu > MENUINDEX0 && menu < MENUINDEX4) {
                System.out.println("Square meters: ");
                double squareMeters = sc.nextDouble();
                System.out.println("Distance to kindergarten(km): ");
                double distanceToKindergarten = sc.nextDouble();
                System.out.println("Distance to school(km): ");
                double distanceToSchool = sc.nextDouble();
                System.out.println("Distance to center(km): ");
                double distanceToCenter = sc.nextDouble();
                System.out.println("distance to mall(km): ");
                double distanceToMall = sc.nextDouble();
                System.out.println("Price: ");
                double price = sc.nextDouble();
                System.out.println("Rooms: ");
                double room = sc.nextDouble();
                if (menu == MENUINDEX1) {
                    System.out.println("Floor: ");
                    int floor = sc.nextInt();
                    controller.addFlat(squareMeters, distanceToKindergarten,
                            distanceToSchool, distanceToCenter,
                            distanceToMall, price, room, floor);
                }
                if (menu == MENUINDEX2) {
                    System.out.println("Number of floors: ");
                    int floor = sc.nextInt();
                    controller.addPenthaus(squareMeters, distanceToKindergarten,
                            distanceToSchool, distanceToCenter,
                            distanceToMall, price, room, floor);
                }
                if (menu == MENUINDEX3) {
                    System.out.println("Numbers of floors: ");
                    int numbersOfFloors = sc.nextInt();
                    System.out.println("Numbers of garage places");
                    int garageNumber = sc.nextInt();
                    controller.addMansion(squareMeters, distanceToKindergarten,
                            distanceToSchool, distanceToCenter,
                            distanceToMall, price, room, numbersOfFloors,
                            garageNumber);
                }
            } else if (menu == MENUINDEX4) {
                if (autoMode) {
                    sc = new Scanner(System.in);
                    System.out.println();
                    System.out.println("Automode is not working now");
                    System.out.println();
                    autoMode = false;
                }
                System.out.println("1.Flat");
                System.out.println("2.Penthouse");
                System.out.println("3.Mansion");
                menu = sc.nextInt();
                switch (menu) {
                    case MENUINDEX1:
                        list = controller.getFlat();
                        break;
                    case MENUINDEX2:
                        list = controller.getPenthouse();
                        break;
                    case MENUINDEX3:
                        list = controller.getMansion();
                        break;
                    default:
                        System.out.println("Error. Wrong input.");
                }
                if (menu < MENUINDEX4 && menu > MENUINDEX0) {
                    secondMenu(menu);
                }
            } else if (menu == MENUINDEX5) {
                firstmenu = false;
            } else {
                System.out.println("Error. Wrong input.");
            }
        }
    }

    /**
     * secondMenu.
     *
     * @param menu - index of menu
     */
    public final void secondMenu(final int menu) {
        boolean secondMenu = true;
        while (secondMenu) {
            System.out.println("1.Square meters");
            System.out.println("2.Distance to kindergarten");
            System.out.println("3.Distance to school");
            System.out.println("4.Distance to center");
            System.out.println("5.Distance to mall");
            System.out.println("6.Price");
            System.out.println("7.Rooms");
            if (menu == MENUINDEX1 || menu == MENUINDEX2) {
                System.out.println("8.Floor");
                System.out.println("9.Show");
            } else {
                System.out.println("8.Numbers of floors: ");
                System.out.println("9.Numbers of garage places");
                System.out.println("10.Show");
            }
            int menu1 = sc.nextInt();
            if (menu1 == MENUINDEX1) {
                System.out.println("Enter square meters:");
                parameters("squareMeters");
            } else if (menu1 == MENUINDEX2) {
                System.out.println("Distance to kindergarten(km):");
                parameters("kindergarten");
            } else if (menu1 == MENUINDEX3) {
                System.out.println("Distance to school(km):");
                parameters("school");
            } else if (menu1 == MENUINDEX4) {
                System.out.println("Distance to center(km):");
                parameters("center");
            } else if (menu1 == MENUINDEX5) {
                System.out.println("Distance to mall(km):");
                parameters("mall");
            } else if (menu1 == MENUINDEX6) {
                System.out.println("Enter the price:");
                parameters("price");
            } else if (menu1 == MENUINDEX7) {
                System.out.println("Enter number of rooms:");
                parameters("room");
            } else if (menu1 == MENUINDEX8) {
                if (menu == MENUINDEX1 || menu == MENUINDEX2) {
                    System.out.println("Enter the floor:");
                    parameters("floor");
                } else {
                    System.out.println("Enter number of floors:");
                    parameters("numbersOfFloors");
                }
            } else if (menu == MENUINDEX3) {
                if (menu1 == MENUINDEX9) {
                    System.out.println("Enter number of garage"
                            + " places:");
                    parameters("garage");
                }
                if (menu1 == MENUINDEX10) {
                    show();
                    secondMenu = false;
                }
            } else if (menu1 == MENUINDEX9) {
                show();
                secondMenu = false;
            } else {
                System.out.println("Error. Wrong input.");
            }
        }
    }

    /**
     * Show - show result.
     */
    public final void show() {
        if (list.size() <= 0) {
            System.err.println("There are no houses found.");
        } else {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
            }
        }
    }

    /**
     * @param str - feature
     */
    public final void parameters(final String str) {
        ArrayList<House> list2 = new ArrayList<House>();
        double count = sc.nextInt();
        list2.clear();
        list2 = controller.orderedBySomeFeature(list, str, count);
        list.clear();
        list = list2;
    }
}

