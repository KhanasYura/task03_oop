package com.khanas.controller;

import com.khanas.model.House;

import java.util.ArrayList;

/**
 * Interface.
 */
public interface ControllerInter {
    /**
     * Mansion - constructor.
     * @param squareMeters - info about how many
     *                       square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param numbersOfFloors - info about the number;
     *                          of floors mansion has
     * @param garageNumber - info about the number of
     *                       parking spaces
     */
    void addMansion(final double squareMeters,
                    final double distanceToKindergarten,
                    final double distanceToSchool,
                    final double distanceToCenter,
                    final double distanceToMall,
                    final double price, final double room,
                    final int numbersOfFloors,
                    final int garageNumber);
    /**
     * Penthouse - constructor.
     * @param squareMeters - info about how many
     *                       square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param floor -info;
     *               on which floor penthouse is located.
     */
    void addPenthaus(final double squareMeters,
                                  final double distanceToKindergarten,
                                  final double distanceToSchool,
                                  final double distanceToCenter,
                                  final double distanceToMall,
                                  final double price,
                                  final double room, final int floor);
    /**
     * Flat - constructor.
     * @param squareMeters - info about how many
     *                    square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param floor -info;
     *               on which floor flat is located.
     */
    void addFlat(final double squareMeters,
                 final double distanceToKindergarten,
                 final double distanceToSchool,
                 final double distanceToCenter,
                 final double distanceToMall,
                 final double price,
                 final double room, final int floor);
    /**
     * getMansion.
     * @return list with mansions
     */
    ArrayList<House> getMansion();
    /**
     * getFlat.
     * @return list with flats
     */
    ArrayList<House> getFlat();
    /**
     * getPenthouse.
     * @return list with penthouses
     */
    ArrayList<House> getPenthouse();
    /**
     * orderedBySomeFeature.
     * @param house - list with houses
     * @param feature - name of feature
     * @param count - some value
     * @return list with houses
     */
    ArrayList<House> orderedBySomeFeature(
            final ArrayList<House> house, final String feature,
            final double count);
}
