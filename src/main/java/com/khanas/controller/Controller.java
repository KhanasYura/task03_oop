package com.khanas.controller;


import com.khanas.model.Flat;
import com.khanas.model.House;
import com.khanas.model.Mansion;
import com.khanas.model.Penthouse;

import java.util.ArrayList;

/**
 * Controller class which is part of MVC pattern.
 * It connects models with view.
 */
public class Controller implements ControllerInter{
    /**
     * list - contains all the information about houses.
     */
    private ArrayList<House> list = new ArrayList<House>();

    /**
     * Flat - constructor.
     * @param squareMeters - info about how many
     *                    square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param floor -info;
     *               on which floor flat is located.
     */
    @Override
    public final void addFlat(final double squareMeters,
                        final double distanceToKindergarten,
                        final double distanceToSchool,
                        final double distanceToCenter,
                        final double distanceToMall,
                        final double price,
                        final double room, final int floor) {
        House flat = new Flat(squareMeters, distanceToKindergarten,
                distanceToSchool, distanceToCenter, distanceToMall,
                price, room, floor);
        list.add(flat);
    }

    /**
     * Penthouse - constructor.
     * @param squareMeters - info about how many
     *                       square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param floor -info;
     *               on which floor penthouse is located.
     */
    @Override
    public final void addPenthaus(final double squareMeters,
                            final double distanceToKindergarten,
                            final double distanceToSchool,
                            final double distanceToCenter,
                            final double distanceToMall,
                            final double price,
                            final double room, final int floor) {
        House penthouse = new Penthouse(squareMeters, distanceToKindergarten,
                distanceToSchool, distanceToCenter, distanceToMall,
                price, room, floor);
        list.add(penthouse);
    }

    /**
     * Mansion - constructor.
     * @param squareMeters - info about how many
     *                       square meters have the penthouse
     * @param distanceToKindergarten - info about distance
     *                                 to kindergarten
     * @param distanceToSchool - info about distance
     *                           to school
     * @param distanceToCenter - info about distance
     *                           to center
     * @param distanceToMall - info about distance
     *                         to mall
     * @param price - info about the price of penthouse
     * @param room - info about number of rooms
     * @param numbersOfFloors - info about the number;
     *                          of floors mansion has
     * @param garageNumber - info about the number of
     *                       parking spaces
     */
    @Override
    public final void addMansion(final double squareMeters,
                           final double distanceToKindergarten,
                           final double distanceToSchool,
                           final double distanceToCenter,
                           final double distanceToMall,
                           final double price, final double room,
                           final int numbersOfFloors,
                           final int garageNumber) {
        House mansion = new Mansion(squareMeters,
                distanceToKindergarten, distanceToSchool,
                distanceToCenter, distanceToMall, price,
                room, numbersOfFloors, garageNumber);
        list.add(mansion);
    }

    /**
     * getFlat.
     * @return list with flats
     */
    @Override
    public final ArrayList<House> getFlat() {
        ArrayList<House> flat = new ArrayList<House>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) instanceof Flat) {
                flat.add(list.get(i));
            }
        }
        return flat;
    }

    /**
     * getMansion.
     * @return list with mansions
     */
    @Override
    public final ArrayList<House> getMansion() {
        ArrayList<House> mansion = new ArrayList<House>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) instanceof Mansion) {
                mansion.add(list.get(i));
            }
        }
        return mansion;
    }

    /**
     * getPenthouse.
     * @return list with penthouses
     */
    @Override
    public final ArrayList<House> getPenthouse() {
        ArrayList<House> penthouse = new ArrayList<House>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) instanceof Penthouse) {
                penthouse.add(list.get(i));
            }
        }
        return penthouse;
    }

    /**
     * orderedBySomeFeature.
     * @param house - list with houses
     * @param feature - name of feature
     * @param count - some value
     * @return list with houses
     */
    @Override
    public final ArrayList<House> orderedBySomeFeature(
            final ArrayList<House> house, final String feature,
            final double count) {
        ArrayList<House> newHouse = new ArrayList<House>();
        for (int i = 0; i < house.size(); i++) {
            if (feature.equals("price") && house.get(i).getPrice() <= count) {
                    newHouse.add(house.get(i));
            }
            if (feature.equals("kindergarten")
                    && house.get(i).getDistanceToKindergarten() <= count) {
                    newHouse.add(house.get(i));
            }
            if (feature.equals("center") && house.get(i).getDistanceToCenter()
                    <= count) {
                    newHouse.add(house.get(i));
            }
            if (feature.equals("school") && house.get(i).getDistanceToSchool()
                    <= count) {
                    newHouse.add(house.get(i));
            }
            if (feature.equals("squareMeters") && house.get(i).getSquareMeters()
                    <= count) {
                    newHouse.add(house.get(i));
            }
            if (feature.equals("mall") && house.get(i).getDistanceToMall()
                    <= count) {
                    newHouse.add(house.get(i));
            }
            if (feature.equals("room") && house.get(i).getRoom() >= count) {
                    newHouse.add(house.get(i));
            }
            if (feature.equals("garage")) {
                Mansion mansion = (Mansion) house.get(i);
                if (mansion.getGarageNumber() == count) {
                    newHouse.add(mansion);
                }
            }
            if (feature.equals("floor")) {
                if (house.get(i) instanceof Penthouse) {
                    Penthouse penthouse = (Penthouse) house.get(i);
                    if (penthouse.getFloor() == count) {
                        newHouse.add(penthouse);
                    }
                } else if (house.get(i) instanceof Flat) {
                    Flat flat = (Flat) house.get(i);
                    if (flat.getFloor() == count) {
                        newHouse.add(flat);
                    }
                }
            }
            if (feature.equals("numbersOfFloors")) {
                Mansion mansion = (Mansion) house.get(i);
                if (mansion.getGarageNumber() == count) {
                    newHouse.add(mansion);
                }
            }
        }
        return newHouse;
    }
}

